import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';

import fetch from 'isomorphic-fetch';

global.fetch = fetch;

Enzyme.configure({ adapter: new Adapter() });
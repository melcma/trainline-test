export default async function getApi() {
    const response = await fetch('http://localhost:8002');
    const data = await response.json();
    return data;
};
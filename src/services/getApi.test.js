import getApi from './getApi';

describe('Testing API response from API proxy', () => {
    it('Should return object with data', async () => {
        const response = await getApi();
        expect(response.data.checkedAt).toBe('10:56');
        expect(response.data.callingPoints.length).not.toBe(0);
    })
});
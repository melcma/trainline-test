import React, {Component} from 'react';
import styled from 'react-emotion';

import Station from '../Station/Station';

import getApi from '../../services/getApi';

const StyledTrack = styled('div')`
  display: block;
`;

export async function doFetch() {
    const api = await getApi();
    return await {tracks: api.data};
}

export default class TrainRoute extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tracks: [],
            trainLocation: null
        };

        this.setApiToState();
    }

    async setApiToState() {
        this.setState(await doFetch());
    }

    renderStations() {
        return this.state.tracks.callingPoints.map(station => {
            return (
                <Station stationData={station} key={station.scheduledAt}/>
            )
        });
    }

    render() {
        return (
            <StyledTrack>
                {this.state.tracks.callingPoints && this.renderStations()}
            </StyledTrack>
        )
    }
}
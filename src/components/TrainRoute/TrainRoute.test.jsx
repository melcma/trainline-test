import React from 'react';
import {shallow} from 'enzyme';

import TrainRoute, {doFetch} from '../TrainRoute/TrainRoute';

describe('rendering of <TrainRoute/> component', () => {
    it('<TrainRoute/> renders without crashing', () => {
        const component = shallow(<TrainRoute/>);
    });

    it('<TrainRoute/> matches snapshot', () => {
        const component = shallow(<TrainRoute/>);
        expect(component).toMatchSnapshot();
    });

    it('setApiToState() updates state with new data', async () => {
        const component = shallow(<TrainRoute/>);
        await component.instance().setApiToState();
        const state = component.state();

        expect(state.tracks.checkedAt).toBe('10:56');
        expect(state.tracks.callingPoints.length).not.toBe(0);
    });

    it('renderStations() returns array of <Station/> components', async () => {
        const component = shallow(<TrainRoute/>);
        await component.instance().setApiToState();
        const stations = component.instance().renderStations();

        expect(stations.length).not.toBe(0);
    });

    it('doFetch() fetches data from API and returns an object', async () => {
        const data = await doFetch();

        expect(data.tracks.checkedAt).toBe('10:56');
        expect(data.tracks.callingPoints.length).not.toBe(0);
    });
});

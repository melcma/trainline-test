import React from 'react';
import {shallow} from 'enzyme';
import Station, {Line} from './Station';
import Train from "../Train/Train";

describe('rendering of <Station/> component', () => {
    let component;
    const stationData = {
        "hasDeparted": true,
        "isTrainHere": false,
        "station": "Sevenoaks",
        "scheduledAt": "10:00",
        "estimatedAt": "On time",
        "isOrigin": true,
        "isDestination": false
    };
    it('<Station/> renders without crashing', () => {
        component = shallow(<Station stationData={stationData}/>);
    });

    it('<Station/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });

    it('<Line> should has props {isDestination: true}', () => {
        const line = shallow(<Line isDestination={true}/>);
        expect(line.instance().props).toEqual({isDestination: true});
    })

    it('<Line> should has props {isDestination: false}', () => {
        const line = shallow(<Line isDestination={false}/>);
        expect(line.instance().props).toEqual({isDestination: false});
    })
});
import React from 'react';
import styled from 'react-emotion';

import Train from '../Train/Train';
import colors from '../../theme';

const StationDetails = styled('span')`
  display: inline-block;
  position: relative;
  margin-left: 18px;
  padding-left: 18px;
`;

const StationName = styled('span')`
  display: block;
  font-size: 14px;
  color: ${colors.gray};
  font-weight: 700;
`;

const EstimatedTime = styled('p')`
  margin-top: 2px;
  font-size: 13px;
  color: ${colors.gray};
`;

const ScheduledTime = styled('span')`
  display: inline-block;
  vertical-align: top;
  color: ${colors.gray};
  font-weight: 700;
  font-size: 16px;
`;

export const Line = styled('span')`
  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    display: ${props => props.isDestination ? 'none' : 'block'};
    height: 100%;
    width: 1px;
    background-color: ${colors.gray};
  }
  
  &::before {
    content: '';
    position: absolute;
    top: 12px;
    left: 0;
    z-index: 2;
    width: 7px;
    height: 7px;
    border: 1px solid ${colors.gray};
    border-radius: 50%;
    background-color: white;
    transform: translateX(-50%);
  }
  
  &.is-active {
    &::before {
      width: 12px;
      height: 12px;
      top: 0;
      background-color: black;
    }
  }
`;

const TrainIcon = styled('span')`
  position: absolute;
  top: calc(150% + 3px);
  left: 0;
  z-index: 2;
  border-radius: 50%;
  transform: translateX(-50%);
  background-color: #48d5b5;
  width: 26px;
  height: 26px;
`;


export default function Station({stationData}) {
    const {scheduledAt, isOrigin, isDestination, estimatedAt, isTrainHere, station} = stationData;

    return (
        <div>
            <ScheduledTime>{scheduledAt}</ScheduledTime>
            <StationDetails>
                <Line className={(isOrigin || isDestination) && 'is-active'} isDestination={isDestination}>
                    {isTrainHere && <TrainIcon><Train size='small'/></TrainIcon>}
                </Line>
                <StationName>{station}</StationName>
                <EstimatedTime>{estimatedAt}</EstimatedTime>
            </StationDetails>
        </div>
    )
}
import React from 'react';
import {shallow} from 'enzyme';
import TravelForm from '../TravelForm/TravelForm';

describe('rendering of <TravelForm/> component', () => {
    let component;
    it('<TravelForm/> renders without crashing', () => {
        component = shallow(<TravelForm/>);
    });

    it('<TravelForm/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });
});

import React from 'react';
import styled from 'react-emotion';
import Train from '../Train/Train';
import Title from '../Title/Title';
import Subtitle from '../Subtitle/Subtitle';
import Operator from '../Operator/Operator';

const StyledTravelForm = styled('div')`
  display: inline-block;
`;

const TrainContainer = styled('div')`
  position: relative;
  vertical-align: top;
  display: inline-block;
  width: 40px;
  height: 40px;
  margin-left: 2px;
  margin-right: 20px;
`;

const LocationsContainer = styled('div')`
  display: inline-block;
`;

export default function TravelForm() {
    return (
        <StyledTravelForm>
            <TrainContainer>
                <Train/>
            </TrainContainer>
            <LocationsContainer>
                <Title>Farringdon</Title>
                <br/>
                <Subtitle>to</Subtitle><Title> West Hampstead Thameslink</Title>
                <Operator>Operated by Thameslink</Operator>
            </LocationsContainer>
        </StyledTravelForm>
    )
};
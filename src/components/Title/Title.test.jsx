import React from 'react';
import {shallow} from 'enzyme';
import Title from './Title';

describe('rendering of <Title/> component', () => {
    let component;
    it('<Title/> renders without crashing', () => {
        component = shallow(<Title/>);
    });

    it('<Title/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });
});

import React from 'react';
import styled from 'react-emotion';

const StyledTitle = styled('h2')`
  display: inline;
  margin-top: 0;
  margin-bottom: 0;
  font-size: 20px;
  font-weight: 700;
`;

export default function Title ({children}) {
    return (
        <StyledTitle>{children}</StyledTitle>
    )
}
import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Operator from './Operator';

describe('rendering of <Operator/> component', () => {
    let component;
    it('<Operator/> renders without crashing', () => {
        component = shallow(<Operator/>);
    });

    it('<Operator/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });
});

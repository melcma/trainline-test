import styled from 'react-emotion';
import colors from '../../theme';

const Operator = styled('h4')`
  margin-top: 5px;
  font-size: 14px;
  color: ${colors.gray};
`;

export default Operator;
import React from 'react';
import {shallow} from 'enzyme';
import Subtitle from './Subtitle';

describe('rendering of <Subtitle/> component', () => {
    let component;
    it('<Subtitle/> renders without crashing', () => {
        component = shallow(<Subtitle/>);
    });

    it('<Subtitle/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });
});

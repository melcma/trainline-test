import styled from 'react-emotion';

import colors from '../../theme';

const Subtitle = styled('h3')`
  margin-top: 0;
  color: ${colors.lightGray};
  font-size: 18px;
  display: inline;
`;

export default Subtitle;
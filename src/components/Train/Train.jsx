import React from 'react';
import styled from 'react-emotion';

const StyledTrain = styled('span')`
  position: absolute;
  top: 50%;
  left: 50%;
  display: inline-block;
  width: 20px;
  height: 24px;
  border-radius: 4px;
  border: 2px solid black;
  transform: translate(-50%, -50%);

  &.small {
    transform-origin: top left;
    transform: scale(0.45) translate(-50%, -60%);
  }
`;

const Reflector = styled('span')`
  position: absolute;
  bottom: 4px;
  width: 5px;
  height: 5px;
  background-color: black;
  border-radius: 50%;
`;

const Window = styled('span')`
  position: absolute;
  top: 1px;
  left: 1px;
  width: calc(100% - 6px);
  height: 6px;
  border-radius: 2px;
  border: 2px solid black;
  `;

const LeftReflector = styled(Reflector)`
  left: 2px;
`;

const RightReflector = styled(Reflector)`
  right: 2px;
`;

const Track = styled('span')`
  position: absolute;
  top: 100%;
  display: block;
  width: 2px;
  height: 10px;
  background-color: black;
`;

const LeftTrack = styled(Track)`
  left: 2px;
  transform: rotate(30deg);
`;

const RightTrack = styled(Track)`
  right: 2px;
  transform: rotate(-30deg);
`;

export default function Train({size}) {
    return (
        <StyledTrain className={size}>
            <Window/>
            <LeftReflector/>
            <RightReflector/>
            <LeftTrack/>
            <RightTrack/>
        </StyledTrain>
    )
}
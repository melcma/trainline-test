import React from 'react';
import {shallow} from 'enzyme';
import Train from './Train';

describe('rendering of <Train/> component', () => {
    let component;
    it('<Train/> renders without crashing', () => {
        component = shallow(<Train/>);
    });

    it('<Train/> matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });
});

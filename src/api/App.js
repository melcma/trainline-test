const server = require('./Server');
const port = process.env.PORT || 9002;

server.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
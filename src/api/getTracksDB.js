const fs = require('fs');

function getTracksDB(db) {
    return new Promise((resolve, reject) => {
        fs.readFile(db, (err, data) => {
            if (err) reject('no file found');
            resolve(data);
        });
    });
}

module.exports = getTracksDB;
import request from "supertest";
import server from "./Server";

describe('testing backend API service in node.js', () => {
    it('server responds with JSON data', (done) => {
        request(server)
            .get('/')
            .then(async (response) => {
                const parsedResponse = JSON.parse(response.body);
                expect(response.statusCode).toBe(200);
                expect(parsedResponse.data.checkedAt).toBe('10:56');
                expect(parsedResponse.data.callingPoints.length).not.toBe(0);
                done();
            })
    });
});
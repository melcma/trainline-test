const path = require('path');
const getTracksDB = require('./getTracksDB');
const db = path.resolve(__dirname + '/../../data/ldb.json');

describe('testing backend API service in node.js', () => {
    it('getTracksDB() reads data from json file', async () => {
        const buffer = await getTracksDB(db);
        const response = JSON.parse(buffer.toString());

        expect(response.data.checkedAt).toBe('10:56');
        expect(response.data.callingPoints.length).not.toBe(0);
    });

    it('getTracksDB() fails with wrong path', async () => {
        try {
            const buffer = await getTracksDB(path.resolve(__dirname + '/../../data/wrong.json'));
        } catch (e) {
            expect(e).toEqual('no file found');
        }

    });
});
const path = require('path');
const express = require('express');
const getTracksDB = require('./getTracksDB');

const db = path.resolve(__dirname + '/../../data/ldb.json');

const server = express();

server.get('/', async (req, res) => {
    const data = await getTracksDB(db);

    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.send(data);
});

module.exports = server;
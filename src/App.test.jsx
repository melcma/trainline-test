import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import {toContainReact} from 'jest-enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import App from './App';
import TravelForm from './components/TravelForm/TravelForm';
import TrainRoute from './components/TrainRoute/TrainRoute';

Enzyme.configure({ adapter: new Adapter() });

describe('rendering of <App/> component', () => {
    let app;
    it('<App/> renders without crashing', () => {
        app = shallow(<App/>);
    });

    it('<App/> matches snapshot', () => {
        expect(app).toMatchSnapshot();
    });

    it('<App/> contains <TravelForm/>', () => {
        expect(app).toContainReact(<TravelForm/>);
    });

    it('<App/> contains <TrainRoute/>', () => {
        expect(app).toContainReact(<TrainRoute/>);
    });
});

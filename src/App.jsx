import React, {Component} from 'react';
import {injectGlobal} from 'react-emotion';

import TravelForm from './components/TravelForm/TravelForm';
import TrainRoute from './components/TrainRoute/TrainRoute';

injectGlobal`
  body {
    font-family: 'Arial', sans-serif;
  }`;

class App extends Component {
    render() {
        return (
            <div>
                <TravelForm/>
                <TrainRoute/>
            </div>
        );
    }
}

export default App;
